<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/8/2019
 * Time: 6:31 PM
 */

session_start();

// Flash message helper
// EXAMPLE - flash('register_success', 'You are now registrated');
// DISPLAY IN VIEW - echo flash('register_succ');
function flash($name = '', $message = '', $class = 'alert alert-success') {

    if(!empty($name)) {
        if(!empty($message) && empty($_SESSION[$name])) {
            /*if(!empty($_SESSION[$name])) {
                unset($_SESSION[$name]);
            }*/
           /* if(!empty($_SESSION[$name. '_class'])) {
                unset($_SESSION[$name. '_class']);
            }*/

            $_SESSION[$name] = $message;
            //$_SESSION[$name . '_class'] = $class;

        } elseif(empty($message) && !empty($_SESSION[$name])) {
           // $class = !empty($_SESSION[$name. '_class']) ? $_SESSION[$name. '_class'] : '';
            echo '<div class="' . $class . '" id="msg-flash">' . $_SESSION[$name] . '</div>';
            unset($_SESSION[$name]);
          //  unset($_SESSION[$name. '_class']);
        }
    }

}

function isLoggedIn($role = '') {

    if(!empty($role)) {
        if(isset($_SESSION['user_role']) && $_SESSION['user_role'] == $role) {
            return true;
        } else {
            return false;
        }
    } else {
        if (isset($_SESSION['user_id'])) {
            return true;
        } else {
            return false;
        }
    }

}