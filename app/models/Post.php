<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/9/2019
 * Time: 10:31 PM
 */

class Post {

    private $db;

    public function __construct() {

        $this->db = new Database();
    }


    // Get all posts
   /* public function getPosts() {
        $this->db->query("SELECT * FROM posts");
        $result = $this->db->resultSet();
        return $result;
    }*/


    // Posts by User
    public function getPosts() {
        $this->db->query("SELECT *,
                              posts.id AS postId,
                              users.id AS userId,
                              posts.created_at AS postCreated,
                              users.created_at AS userCreated
                              FROM posts
                              INNER JOIN users
                              ON posts.user_id = users.id
                              ORDER BY posts.created_at DESC
                              ");
        $result = $this->db->resultSet();
        return $result;
    }

    // Add Post
    public function add($data) {
        $this->db->query("INSERT INTO posts(`user_id`,`title`,`body`) VALUES(:userId, :title, :body)");

        // Bind values
        $this->db->bind("userId", $data['user_id']);
        $this->db->bind("title", $data['title']);
        $this->db->bind('body', $data['body']);

        // Execute
        if($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // Get Post by ID
    public function getPostById($id) {
        $this->db->query("SELECT * FROM posts WHERE id = :id");
        $this->db->bind(":id",$id);
        $result = $this->db->single();
        return $result;
    }

    // Update Post
    public function updatePost($data) {
        $this->db->query('UPDATE posts SET title = :title, body = :body WHERE id = :id');

        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':body', $data['body']);

        if($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    // Delete Post by Id
    public function deletePost($postId) {
        $this->db->query('DELETE FROM posts WHERE id = :id');

        //Bind values
        $this->db->bind(':id',$postId);

        if($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }


}