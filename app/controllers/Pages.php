<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/2/2019
 * Time: 6:48 PM
 */

class Pages extends Controller {
    
    public function __construct() {
        $this->postModel = $this->model('Post');
    }

    public function index() {

        if(isLoggedIn()) {
            // redirect to another homepage
        }

        $data = [
            'title' => 'Welcome!',
            'description' => 'This is a site which use for learning english'
        ];

        $this->view('pages/index', $data);

    }
    
    public function about() {

        $data = [
            'title' => 'About',
            'description' => 'We use this site for leaderning english with other people'
        ];

        $this->view('pages/about', $data);
    }

}