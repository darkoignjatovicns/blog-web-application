<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/5/2019
 * Time: 10:08 PM
 */

class Users extends Controller{

    public function __construct() {
        // Load model
        $this->userModel = $this->model('User');
    }


    public function register() {
        // Check for POST
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Porcess form

            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

            // Init data
            $data = [
                'firstName' => trim($_POST['firstName']),
                'lastName' => trim($_POST['lastName']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirmPassword' => trim($_POST['confirmPassword']),
                'firstName_err' => '',
                'lastName_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];

            // Validate First Name
            if(empty($data['firstName'])) {
                $data['firstName_err'] = 'Please enter first name';
            }

            // Validate Last Name
            if(empty($data['lastName'])) {
                $data['lastName_err'] = 'Please enter last name';
            }

            // Validate email
            if(empty($data['email'])) {
                $data['email_err'] = 'Please enter email';
            } else {
                if($this->userModel->findUserByEmail($data['email'])) {
                    $data['email_err'] = 'Email has already taken';
                }
            }

            // Validate password
            if(empty($data['password'])) {
                $data['password_err'] = 'Please enter password';
            } else {
                if(strlen($data['password']) < 6) {
                    $data['password_err'] = 'Password must be at least 6 character';
                }
            }

            // Validate Confirm Password
            if(empty($data['confirmPassword'])) {
                $data['confirmPassword_err'] = 'Please confirm password';
            } else {
                if($data['password'] != $data['confirmPassword']) {
                    $data['confirmPassword_err'] = 'Password does not equal';
                }
            }

            // Make sure errors are empty
            if(empty($data['firstName_err']) && empty($data['lastName_err']) && empty($data['email_err']) &&
                empty($data['password_err']) && empty($data['confirmPassword_err'])) {
                // Validated

                // Hash Password
                $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                if($this->userModel->register($data)) {
                    // Redirect
                    flash('register_success', 'You are registrated and can log in');
                    redirect('users/login');
                } else {
                    die('Something went wrong');
                }

            } else {
                // Load view with errors
                $this->view('users/register',$data);
            }


        } else {
            // Init data
            $data = [
                'firstName' => '',
                'lastName' => '',
                'email' => '',
                'password' => '',
                'confirmPassword' => '',
                'firstName_err' => '',
                'lastName_err' => '',
                'email_err' => '',
                'pasword_err' => '',
                'confirmPassword_err' => ''
            ];

            // Load view
            $this->view('users/register',$data);
        }
    }

    public function login() {
        // Check for POST
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Process form

            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

            // Init data
            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => ''
            ];

            // Validate email
            if(empty($data['email'])) {
                $data['email_err'] = 'Please enter email';
            } else {
                if(!$this->userModel->findUserByEmail($data['email'])) {
                    // User not found
                    $data['email_err'] = 'No user found';
                }
            }

            // Validate password
            if(empty($data['password'])) {
                $data['password_err'] = 'Please enter password';
            }

            /* Check for user/email
            if($this->userModel->findUserByEmail($data['email'])) {
                // User found
            } else {
                // User not found
                $data['email_err'] = 'No user found';
            }*/

            // Make sure errors are empty
            if(empty($data['email_err']) && empty($data['password_err'])) {
                // Validated
                // Check and set logged in user
                $loggedInUser = $this->userModel->login($data['email'], $data['password']);

                if($loggedInUser) {
                    // Create session
                    $this->createUserSession($loggedInUser);
                } else {
                    $data['password_err'] = 'Password incorrect';
                    // Load view with password error
                    $this->view('users/login',$data);
                }
            } else {
                // Load view with errors
                $this->view('users/login', $data);
            }

        } else {
            //Init data
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''
            ];

            // Load view
            $this->view('users/login',$data);
        }
    }

    public function createUserSession($user) {
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_firstName'] = $user->first_name;
        $_SESSION['user_lastName'] = $user->last_name;
        $_SESSION['user_role'] = $user->role;
        redirect('posts ');
    }

    public function logout() {
         unset($_SESSION['user_id']);
         unset($_SESSION['user_email']);
         unset($_SESSION['user_firstName']);
         unset($_SESSION['user_lastName']);
         unset($_SESSION['user_role']);
         session_destroy();
         redirect('users/login');
    }










}