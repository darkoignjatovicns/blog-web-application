<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/4/2019
 * Time: 7:15 PM
 */
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
 <div  class="container">
      <a class="navbar-brand" href="<?php echo URL_ROOT; ?>"><?php echo SITE_NAME; ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">

        <!-- home/about... -->
        <ul class="navbar-nav mr-auto">

          <li class="nav-item active">
            <a class="nav-link" href="<?php echo URL_ROOT; ?>">Home</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL_ROOT; ?>/pages/about">About us</a>
          </li>

          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Grammar</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>

        <!-- Search -->
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
          <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
        </form>

      <!-- LOGIN/REGISTER -->
      <ul class="navbar-nav ml-auto">
          <?php if(isset($_SESSION['user_id'])) : ?>
          <li class="nav-item">
              <a class="nav-link" href="">Welcome, <?php echo $_SESSION['user_firstName']; ?></a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="<?php echo URL_ROOT; ?>/users/logout"> Logout</a>
          </li>
          <?php else : ?>
          <li class="nav-item">
              <a class="nav-link" href="<?php echo URL_ROOT; ?>/users/register" >Register</a>
          </li>

          <li class="nav-item">
              <a class="nav-link" href="<?php echo URL_ROOT; ?>/users/login" >Login</a>
          </li>
          <?php endif; ?>
      </ul>

      </div>
 </div>
</nav>