<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/3/2019
 * Time: 12:46 PM
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie-edge">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- End bootstrap -->

    <!-- Bootstrap CDN  AWSOME FONT -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- End Bootstrap CDN -->

    <!-- My css files for style -->
    <link rel="stylesheet" href="<?php echo URL_ROOT; ?>/css/style.css">
    <!-- End My css files for style -->

    <title><?php echo SITE_NAME; ?></title>
</head>
<body>

    <?php require 'navbar.php'; ?>


<div class="container">

