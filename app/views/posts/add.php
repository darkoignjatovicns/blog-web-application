<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/11/2019
 * Time: 3:37 PM
 */

?>

<a href="<?php echo URL_ROOT; ?>/posts" class="btn btn-light"><i class="fa fa-backward"></i>Back</a>
<div class="card card-body bg-light">
    <h2>Add Post</h2>
    <p>Create post with that form</p>
    <form action="<?php echo URL_ROOT; ?>/posts/add" method="post">

        <!-- Title -->
        <div class="form-group">
            <label for="title">Title: <sup>*</sup></label>
            <input type="text" name="title" class="form-control form-control-lg
            <?php echo (!empty($data['title_err'])) ? 'is-invalid' : ''; ?>"
            value="<?php echo $data['title']; ?>" />
            <span class="invalid-feedback"><?php echo $data['title_err']; ?></span>
        </div>
        <!-- End Title -->

        <!-- Body -->
        <div class="form-group">
            <label for="body">Body: <sup>*</sup></label>
            <textarea name="body" class="form-control form-control-lg
            <?php echo (!empty($data['body_err'])) ? 'is-invalid' : ''; ?>">
            <?php echo $data['body']; ?></textarea>
            <span class="invalid-feedback"><?php echo $data['body_err']; ?></span>
        </div>
        <!-- End Body -->

        <input type="submit" class="btn btn-success" value="Add" />



    </form>
</div>
