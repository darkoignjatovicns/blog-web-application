<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/12/2019
 * Time: 1:32 PM
 */

?>

<a href="<?php echo URL_ROOT; ?>/posts" class="btn btn-light"><i class="fa fa-backward"></i>Back</a>
<h1><?php echo $data['post']->title; ?></h1>
<div class="bg-secondary text-white p-2 mb-3">
    Written by <?php echo $data['user']->first_name . ' ' . $data['user']->last_name; ?>
    On
    <?php echo $data['user']->created_at;  ?>
</div>
<p>
    <?php echo $data['post']->body; ?>
</p>

<!-- That means that post created by that user -->
<?php if($data['user']->id == $_SESSION['user_id']) : ?>
    <hr>
    <a href="<?php echo URL_ROOT; ?>/posts/edit/<?php echo $data['post']->id; ?>" class="btn btn-dark">Edit</a>

    <form class="pull-right" action="<?php echo URL_ROOT; ?>/posts/delete/<?php echo $data['post']->id; ?>" method="post">
        <input type="submit" value="delete" class="btn btn-danger" />
    </form>

<?php endif; ?>

