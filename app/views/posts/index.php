<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/9/2019
 * Time: 9:21 PM
 */

?>

<?php flash('post_message'); ?>
<div class="row mb-3" >

    <div class="col-md-6">
        <h1>Posts</h1>
    </div>

    <div class="col-md-6">
        <a href="<?php echo URL_ROOT; ?>/posts/add" class="btn btn-primary pull-right">
            <i class="fa fa-pencil"></i> Add Post
        </a>
    </div>
</div>
    <?php foreach($data['posts'] as $post) : ?>
        <div class="card card-body mb-3">

            <!-- Post Title -->
            <h4 class="card-title"><?php echo $post->title; ?></h4>

            <!-- -->
            <div class="bg-light p-2 mb-3">
                Written by <?php echo $post->first_name . ' ' . $post->last_name . ' on ' . $post->postCreated;  ?>
            </div>

            <!-- Post Body -->
            <p class="card-text"><?php echo $post->body; ?></p>
            <a href="<?php echo URL_ROOT; ?>/posts/show/<?php echo $post->postId; ?>" class="btn btn-dark">More</a>


        </div>
    <?php endforeach; ?>

