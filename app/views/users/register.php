<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/5/2019
 * Time: 10:07 PM
 */?>


<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt5 ">
            <h2>Create An Account</h2>
            <p>Please fill out this form to register with us</p>
            <form action="<?php echo URL_ROOT; ?>/users/register" method="post">

                <!-- First Name -->
                <div class="form-group">
                    <label for="firstName"> First name: <sup>*</sup></label>
                    <input type="text" name="firstName" class="form-control form-control-lg
                       <?php echo (!empty($data['firstName_err'])) ? 'is-invalid' : ''; ?>"
                       value="<?php echo $data['firstName']; ?>"
                    />
                    <span class="invalid-feedback"><?php echo $data['firstName_err']; ?></span>
                </div>
                <!-- End First Name -->

                <!-- Last Name -->
                <div class="form-group">
                    <label for="lastName">Last name: <sup>*</sup></label>
                    <input type="text" name="lastName" class="form-control form-control-lg
                        <?php echo (!empty($data['lastName_err'])) ? 'is-invalid' : ''; ?>"
                        value="<?php echo $data['lastName']; ?>"
                    />
                    <span class="invalid-feedback"><?php echo $data['lastName_err']; ?></span>
                </div>

                <!-- Email -->
                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="text" name="email" class="form-control form-control-lg
                        <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>"
                        value="<?php echo $data['email']; ?>"
                    />
                    <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                </div>
                <!-- End Email -->

                <!-- Password -->
                <div class="form-group">
                    <label for="password">Password: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control form-control-lg
                        <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>"
                        value="<?php echo $data['password']; ?>"
                    />
                    <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                </div>
                <!-- End password -->

                <!-- Confirm Password -->
                <div class="form-group">
                    <label for="confirmPassword">Confirm password: <sup>*</sup></label>
                    <input type="password" name="confirmPassword" class="form-control form-control-lg
                    <?php echo (!empty($data['confirmPassword_err'])) ? 'is-invalid' : ''; ?>"
                    value="<?php echo $data['confirmPassword']; ?>"
                    />
                    <span class="invalid-feedback"><?php echo $data['confirmPassword_err']; ?></span>
                </div>
                <!-- End Confirm Password -->

                <!-- Submit -->
                <div class="row">
                    <div class="col">
                        <input type="submit" value="Register" class="btn btn-success btn-block">
                    </div>
                    <div class="col">
                        <a href="<?php echo URL_ROOT; ?>/users/login" class="btn btn-light btn-block">
                            Have an account? Login
                        </a>
                    </div>
                </div>
                <!-- End submit -->



            </form>
        </div>
    </div>
</div>
