<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/6/2019
 * Time: 10:27 PM
 */

?>


<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt5">
            <?php flash('register_success'); ?>
            <h2>Login</h2>
            <p>Please fill in your credentials to log in</p>

            <form action="<?php echo URL_ROOT; ?>/users/login" method="post">

                <!-- Email -->
                <div class="form-group">
                    <label for="email"> Email: <sup>*</sup></label>
                    <input type="text" name="email" class="form-control form-control-lg
                        <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>"
                        value="<?php echo $data['email']; ?>"/>
                    <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                </div>
                <!-- End Email -->


                <!-- Password -->
                <div class="form-group">
                    <label for="password"> Password: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control form-control-lg
                        <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>"
                        value="<?php echo $data['password']; ?>"
                    />
                    <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                </div>
                <!-- End Password -->

                <!-- Submit -->
                <div class="row">
                    <div class="col">
                        <input type="submit" class="btn btn-success btn-block" value="Login" />
                    </div>
                    <div class="col">
                        <a href="<?php echo URL_ROOT; ?>/users/register" class="btn btn-light btn-block">
                           No account? Register
                        </a>
                    </div>
                </div>
                <!-- End Submit -->

            </form>

        </div>
    </div>
</div>
