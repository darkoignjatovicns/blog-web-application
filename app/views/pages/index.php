<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/3/2019
 * Time: 11:58 AM
 */

?>

<div class="jumbotron jumbotron-fluid text-center">
    <div class="container">
        <h1 class="display-3"><?php echo $data['title']; ?></h1>
        <p class="lead"><?php echo $data['description']; ?></p>
    </div>
</div>


