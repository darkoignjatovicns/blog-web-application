<?php
/**
 * Created by PhpStorm.
 * User: dark021
 * Date: 1/3/2019
 * Time: 12:07 PM
 */

// DB Params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'di.english');


// App Root
define('APP_ROOT', dirname(dirname(__FILE__)));

// URL Root
define('URL_ROOT', 'http://localhost/DI.English');

// Site Name
define('SITE_NAME', 'English For Us');

// App version
define('APP_VERSION', '1.0.0');